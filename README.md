# serialio

    import "gitlab.com/JWLAB/serialio"

Package serialio is a simple library created to interact with serial terminals.

## Usage

#### func  IsTerm

```go
func IsTerm(fd int) bool
```
IsTerm returns true if the given file descriptor is a terminal.

#### func  RestoreTerm

```go
func RestoreTerm(fd int, state *State) error
```
RestoreTerm restores the terminal with the given file descriptor to a previous
state.

#### type State

```go
type State struct {
}
```

State contains the state of a serial terminal.

#### func  ConfigTerm

```go
func ConfigTerm(fd int, baud int) (*State, error)
```
ConfigTerm configures the serial terminal with common options options and set
baud rate.
