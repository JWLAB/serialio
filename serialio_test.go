// +build !windows

package serialio

import (
	"syscall"
	"testing"
	"unsafe"

	. "github.com/smartystreets/goconvey/convey"
)

func TestIsTerm(t *testing.T) {

	sysctl = func(fd, call, termp uintptr) syscall.Errno {
		if fd != 0 {
			return 1
		}
		return 0
	}

	Convey("Given a file descriptor for a terminal.", t, func() {

		Convey("If valid terminal return true.", func() {
			fd := 0
			So(IsTerm(fd), ShouldBeTrue)
		})

		Convey("If non-valid terminal return false.", func() {
			fd := 1
			So(IsTerm(fd), ShouldBeFalse)
		})
	})

}

func TestFixBaud(t *testing.T) {

	Convey("Given an input baud rate as an integer.", t, func() {

		Convey("When a supported baud rate is passed.", func() {

			Convey("A valid termios constant is returned.", func() {
				for _, vb := range validBauds {
					// t.Logf("Baud: %d", vb.inBaud)
					So(fixBaud(vb.inBaud), ShouldEqual, vb.outBaud)
				}
			})
		})

		Convey("When an unsupported baud rate is passed.", func() {

			Convey("The termios constant for 9600 baud is returned.", func() {
				for _, bb := range badBauds {
					// t.Logf("Baud: %d", bb.inBaud)
					So(fixBaud(bb.inBaud), ShouldEqual, bb.outBaud)
				}
			})

		})

	})

}

func TestConfigTerm(t *testing.T) {
	var oldState, newState State

	sysctl = func(fd, call, termp uintptr) syscall.Errno {
		if fd != 0 {
			return 1
		}
		if call == sioTermiosRead {
			inState := (*syscall.Termios)(unsafe.Pointer(termp))
			*inState = oldState.termios
		}
		if call == sioTermiosWrite {
			inState := (*syscall.Termios)(unsafe.Pointer(termp))
			newState.termios = *inState
		}
		return 0
	}

	Convey("Given a file descriptor and baud rate.", t, func() {

		fd := 0
		baud := 19200
		origState, _ := ConfigTerm(fd, baud)

		Convey("The original state is returned.", func() {
			So(*origState, ShouldResemble, oldState)
		})
		Convey("The new state is configured.", func() {
			So(newState, ShouldNotResemble, oldState)
			So(newState.termios.Ispeed, ShouldEqual, syscall.B19200)
			So(newState.termios.Ospeed, ShouldEqual, syscall.B19200)
		})
		Convey("The saved state is restored.", func() {
			RestoreTerm(fd, origState)
			So(*origState, ShouldResemble, oldState)
		})

	})

}
