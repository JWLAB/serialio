// +build !windows

package serialio

import (
	"syscall"
	"unsafe"
)

// State contains the state of a serial terminal.
type State struct {
	termios syscall.Termios
}

var sysctl = func(fd, call, termp uintptr) syscall.Errno {
	_, _, e := syscall.Syscall(syscall.SYS_IOCTL, fd, call, termp)
	return e
}

// IsTerm returns true if the given file descriptor is a terminal.
func IsTerm(fd int) bool {
	var termios syscall.Termios
	err := sysctl(uintptr(fd), uintptr(sioTermiosRead), uintptr(unsafe.Pointer(&termios)))
	return err == 0
}

// ConfigTerm configures the serial terminal with common options options and set baud rate.
func ConfigTerm(fd int, baud int) (*State, error) {
	var origState State
	if err := sysctl(uintptr(fd), uintptr(sioTermiosRead), uintptr(unsafe.Pointer(&origState.termios))); err != 0 {
		return nil, err
	}

	newState := origState.termios
	newState.Iflag &^= syscall.BRKINT | syscall.ISTRIP | syscall.IGNCR | syscall.IXON | syscall.IXOFF | syscall.PARMRK
	newState.Lflag &^= syscall.ECHO | syscall.ECHOE | syscall.ICANON | syscall.ISIG | syscall.PARENB
	newState.Oflag &^= syscall.OPOST
	newState.Ispeed = fixBaud(baud)
	newState.Ospeed = fixBaud(baud)
	if err := sysctl(uintptr(fd), uintptr(sioTermiosWrite), uintptr(unsafe.Pointer(&newState))); err != 0 {
		return nil, err
	}

	return &origState, nil
}

// RestoreTerm restores the terminal with the given file descriptor to a
// previous state.
func RestoreTerm(fd int, state *State) error {
	err := sysctl(uintptr(fd), uintptr(sioTermiosWrite), uintptr(unsafe.Pointer(&state.termios)))
	return err
}
